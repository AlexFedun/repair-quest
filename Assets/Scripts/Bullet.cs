﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int Damage = 10;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject obj = collision.gameObject;

        switch (obj.tag)
        {
            case "Tiles":
                break;
            case "Enemy":
                obj.GetComponent<EnemyObject>().enemy.Health -= Damage;
                break;
            case "Player":
                obj.GetComponent<Player>().ChangeEnergy(-Damage);
                Destroy(gameObject);
                break;
            case "Boss":
                Debug.Log("boss");
                break;
            default:
                break;
        }
    }

}
