﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Market : MonoBehaviour
{
    [SerializeField]
    private bool CanOpen = false;
    public GameObject ShopPanel;
    // Start is called before the first frame update
    void Start()
    {
        ShopPanel = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().ShopPanel;
    }

    // Update is called once per frame
    void Update()
    {
        if (CanOpen && Input.GetKeyDown(KeyCode.E))
        {
            ShopPanel.SetActive(!ShopPanel.activeSelf);
        } 
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            CanOpen = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            CanOpen = false;
            ShopPanel.SetActive(false);
        }
    }
}
