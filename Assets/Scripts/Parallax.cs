﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public GameObject Camera;
    public GameObject[] layers;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 camPos = Camera.transform.position;
        layers[0].transform.position = new Vector3(camPos.x * 0.95f, camPos.y * 0.95f);
        layers[1].transform.position = new Vector3(camPos.x * 0.9f, camPos.y * 0.9f + 1);
        layers[2].transform.position = new Vector3(camPos.x * 0.9f, camPos.y * 0.9f);
    }
}
