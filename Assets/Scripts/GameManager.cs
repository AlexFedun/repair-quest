﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.UI;
using UnityEngine.Tilemaps;

public class GameManager : MonoBehaviour
{
    //Settings
    public int width = 4, heigth = 4, distance = 12;
    public int GameOver = 0, LastValue = 0;

    // UI
    public RectTransform energyBar;
    public Text coinsText;
    public Text itemsText;
    public GameObject ShopPanel;
    public GameObject MainMenu;
    public Text damageText, speedText, maxEnergyText;
    public Sprite[] lightSp;
    public InputField input;
    // Main objects
    public GameObject[] chunks;
    public GameObject world;
    public CinemachineVirtualCamera cinemachineVirtualCamera;
    public GameObject playerObj;
    public GameObject PlayerPrefab;
    public Player player;
    public GameObject bulletPrefab;
    public Tilemap border;
    public TileBase tileBase;
    public Animator mainmenu;
    public GameObject[] menuElements;
    public GameObject Win;
    public GameObject Lose;

    // Start is called before the first frame update
    void Start()
    {
        //StartGame();
    }

    // Update is called once per frame
    void Update()
    {
        if (LastValue != GameOver)
        {
            StartCoroutine("OpenMenu");
            Destroy(playerObj);
            foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Tiles"))
            {
                if (obj.name != "BorderTilemap")
                    GameObject.Destroy(obj);
            }
            LastValue = GameOver;
        }
    }

    public void StartGame()
    {
        GameOver = 0;
        LastValue = 0;
        GameOver = 0;
        GenerateLevel();
        playerObj = Instantiate(PlayerPrefab);
        cinemachineVirtualCamera.Follow = playerObj.transform;
        player = playerObj.GetComponent<Player>();
        player.energyBar = energyBar;
        player.itemsText = itemsText;
        player.coinsText = coinsText;
        player.bulletPrefab = bulletPrefab;
        player.gameManager = this;
        ChangeDamage(10);
        ChangeMaxEnergy(10);
        ChangeSpeed(10);
        player.ChangeCoins(0);
        itemsText.text = "0/3";
        StartCoroutine("CloseMenu");
    }

    public void GenerateLevel()
    {
        if(input.text != "")
            Random.InitState(input.text.GetHashCode());
        HashSet<int> numbers = new HashSet<int>();
        while(numbers.Count < 3)
        {
            numbers.Add(Random.Range(1, width * heigth));
        }

        for(int i = -8; i < width * 12 + 10; i++)
        {
            for(int j = -8; j < heigth * 12 + 10; j++)
            {
                if (i <= 0 || i >= width * 12 + 1 || j <= 0 || j >= heigth * 12 + 1)
                {
                    border.SetTile(new Vector3Int(i - 6, j - 7, 0), tileBase);
                }
            }
        }
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Tiles"))
        {
            if(obj.name != "BorderTilemap")
                GameObject.Destroy(obj);
        }
        GameObject bd = Instantiate(border.gameObject, world.transform);
        bd.GetComponent<TilemapCollider2D>().enabled = true;
        int count = 0;
        int finish = Random.Range(1, 3), mX = Random.Range(0, width), mY = Random.Range(1, heigth-1);
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < heigth; j++)
            {
                GameObject obj = world;
                if (i == 0 && j == 0)
                {
                    Instantiate(chunks[0], world.transform);
                }
                else if (i == finish && j == 3)
                {
                    obj = Instantiate(chunks[2], world.transform);
                }
                else if (i == mX && j == mY)
                {
                    obj = Instantiate(chunks[1], world.transform);
                }
                else
                {
                    obj = Instantiate(chunks[Random.Range(3, chunks.Length)], world.transform);
                }
                obj.transform.position = new Vector3(i * distance, j * distance, 1);
                if (numbers.Contains(i * width + j))
                {
                    GameObject temp = obj.transform.Find("Repair item").gameObject;
                    temp.SetActive(true);
                    temp.GetComponent<SpriteRenderer>().sprite = lightSp[count];
                    count++;
                }
            }
        }
    }

    public void ChangeSpeed(int value)
    {
        speedText.text = value.ToString();
    }

    public void ChangeDamage(int value)
    {
        damageText.text = value.ToString();
    }
   
    public void ChangeMaxEnergy(int value)
    {
        maxEnergyText.text = value.ToString();
    }

    public void BuyEnergy()
    {
        if (player.Coins >= 1)
        {
            player.ChangeEnergy(5);
            player.ChangeCoins(-1);
        }
    }
    public void UpgradeSpeed()
    {
        if (player.Coins >= player.SpeedUpdated * 10 + 10)
        {
            player.ChangeCoins(-(player.SpeedUpdated * 10 + 10));
            player.ChangeSpeed(0.25f);
            player.SpeedUpdated++;
            ChangeSpeed(player.SpeedUpdated * 10 + 10);
        }
    }

    public void UpgradeDamage()
    {
        if (player.Coins >= player.DamageUpdated * 10 + 10)
        {
            player.ChangeCoins(-(player.DamageUpdated * 10 + 10));
            player.ChangeDamage(5);
            player.DamageUpdated++;
            ChangeDamage(player.DamageUpdated * 10 + 10);
        }
    }

    public void UpgradeEnergy()
    {
        if (player.Coins >= player.MaxEnergyUpdated * 10 + 10)
        {
            player.ChangeCoins(-(player.MaxEnergyUpdated * 10 + 10));
            player.MaxEnergy += 25;
            player.MaxEnergyUpdated++;
            ChangeMaxEnergy(player.MaxEnergyUpdated * 10 + 10);
        }
    }

    IEnumerator OpenMenu()
    {
        MainMenu.SetActive(true);
        mainmenu.Play("Close");
        yield return new WaitForSeconds(.5f);
        foreach (GameObject obj in menuElements)
        {
            obj.SetActive(true);
        }
        if (GameOver == 1)
        {
            Win.SetActive(true);
            Lose.SetActive(false);
        }
        else
        if (GameOver == -1)
        {
            Win.SetActive(false);
            Lose.SetActive(true);
        }
    }

    IEnumerator CloseMenu()
    {
        foreach (GameObject obj in menuElements)
        {
            obj.SetActive(false);
        }
        mainmenu.Play("MainMenu");
        yield return new WaitForSeconds(.5f);
        MainMenu.SetActive(false);
    }
}
