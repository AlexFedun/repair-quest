﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Player : MonoBehaviour
{
    // Stats
    public int Energy = 100;
    public int MaxEnergy = 100;
    public int MaxEnergyUpdated = 0;
    public double Speed = 1;
    public int SpeedUpdated = 0;
    public int Damage = 10;
    public int DamageUpdated = 0;

    public int Coins = 0;
    public int RepairItems = 0;

    // UI
    public RectTransform energyBar;
    public Text coinsText;
    public GameObject bulletPrefab;
    public GameManager gameManager;
    public Text itemsText;

    //Settings
    public float speedValue = 0.01f;
    public float jumpvalue = 1f;
    public float lerpValueRun = 1f;
    public float lerpValueStop = 1f;

    private Animator animator;
    private Rigidbody2D rg;
    private float xvalue = 0;

    private float timeToNextShot = 0;

    //Reset jumping
    public Collider2D rejampObj;
    public string reTag = "none";
    private ContactFilter2D resetCFilter = new ContactFilter2D();
    private List<Collider2D> collRes = new List<Collider2D>();

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rg = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        float newXValue = 0f; // куда идем?
        if (Input.GetKey(KeyCode.A))
        {
            //left
            newXValue = -speedValue * (float)Speed;
            transform.localScale = new Vector3(-1, 1, 1);
        }
        if (Input.GetKey(KeyCode.D))
        {
            //right
            newXValue = speedValue * (float)Speed;
            transform.localScale = new Vector3(1, 1, 1);
        }

        if(newXValue == 0f)
        {
            xvalue = Mathf.Lerp(xvalue, newXValue, lerpValueStop);
        }
        else
        {
            xvalue = Mathf.Lerp(xvalue, newXValue, lerpValueRun);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            CheckJump();
        }


            if (Input.GetKey(KeyCode.Mouse0))
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                Fire();
            }
        }
    }

    public void FixedUpdate()
    {
        GoMovingX(xvalue);
        if(xvalue > 1 || xvalue < -1)
        {
            animator.Play("Player");
        } else
        {
            animator.Play("Idle");
        }
    }

    private void GoMovingX(float value)
    {
        //this.transform.position += new Vector3(value, 0, 0);
        rg.velocity = new Vector2(value, rg.velocity.y);
    }

    private void GoUp()
    {
        rg.AddForce(transform.up * jumpvalue, ForceMode2D.Impulse);
        ChangeEnergy(-1);
    }

    public void ChangeEnergy(int value = -1)
    {
        if (Energy + value > MaxEnergy)
        {
            Energy = MaxEnergy;
        }
        else
        {
            Energy += value;
        }
        energyBar.offsetMax = new Vector2((float)Energy / MaxEnergy * 100 * 5 - 500, energyBar.offsetMax.y);
        if(Energy < 1)
        {
            gameManager.GameOver = -1;
        }
    }

    public void ChangeSpeed(double value = 0.1)
    {
        Speed += value;
    }


    public void ChangeDamage(int value = 1)
    {
        Damage += value;
    }
    
    public void ChangeCoins(int value = 1)
    {
        Coins += value;
        coinsText.text = Coins.ToString();
    }

    public void ChangeItems(int value = 1)
    {
        RepairItems += value;
        itemsText.text = RepairItems + "/3";
    }

    void Fire()
    {

        if (Time.time - timeToNextShot >= 1f)
        {
            timeToNextShot = Time.time;
            GameObject bullet = (GameObject)Instantiate(bulletPrefab);
            bullet.transform.position = transform.position;
            Physics2D.IgnoreCollision(bullet.GetComponent<Collider2D>(), GetComponent<Collider2D>());
            Vector3 moveDirection = (Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position);
            moveDirection.z = 0;
            moveDirection.Normalize();

            bullet.GetComponent<Rigidbody2D>().velocity = moveDirection * 20;
            bullet.GetComponent<Bullet>().Damage = Damage;
            Destroy(bullet, .7f);
            ChangeEnergy(-5);
        }
    }

    private void CheckJump()
    {
        if (Physics2D.OverlapCollider(rejampObj, resetCFilter, collRes) > 0)
        {
            bool isOnTheGround = false;
            foreach (Collider2D ctemp in collRes)
            {
                if (ctemp.gameObject.tag == reTag)
                {
                    isOnTheGround = true;
                }
            }
            if (isOnTheGround)
                GoUp();
        }
    }
}
