﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
	public int Health = 10;
	public int Damage = 10;
	public float AtackDelay = 0.2f; 
	public bool isLive = true;
	private float timeToNextShot = 0;

	public int rnd;
	public float enemySpeed = 3;
	public float shootDistance = 10;
	public GameObject enemy;
	public GameObject[] Enemys;
	public GameObject leftBarrier;
	public GameObject rightBarrier;
	public GameObject SnowBall;
	public GameObject deadShot;
	public GameObject iceShot;
	private Animator animator;
	public GameObject explosion;

	private GameObject player;
	private Rigidbody2D enemyBody;
	private int enemy_direction = 1;
	private float enemy_width;
	private Vector3 enemy_pos;
	private Vector3 player_pos;
	private float leftBarrier_pos_x;
	private float leftBarrier_width;
	private float rightBarrier_pos_x;
	private float rightBarrier_width;
	private float distance;
	private Vector2 direction;
	private Vector2 player_x_y;
	private Vector2 enemy_x_y;


    void Start()
    {
		timeToNextShot = Time.time;
		rnd = Random.Range(0, 3);
		enemy = Enemys[rnd];
		animator = enemy.GetComponent<Animator>();
		if (rnd == 0)
		{
			Health = 10;
			Damage = 2;
			AtackDelay = 0.2f;
		}
		else if (rnd == 1)
		{
			Health = 15;
			Damage = 15;
			AtackDelay = 2f;
		}
		else if (rnd == 2)
		{
			Health = 30;
			Damage = 30;
			AtackDelay = 5f;
		}

		enemy.SetActive(true);
		player = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().playerObj;
		enemyBody = enemy.GetComponent<Rigidbody2D>();
		enemy_width = enemy.GetComponent<Renderer>().bounds.size.x;
		leftBarrier_pos_x = leftBarrier.transform.position.x;
		leftBarrier_width = leftBarrier.GetComponent<Renderer>().bounds.size.x;
		rightBarrier_pos_x = rightBarrier.transform.position.x;
		rightBarrier_width = rightBarrier.GetComponent<Renderer>().bounds.size.x;
    }

	void Move(Vector3 enemy_pos)
	{
		if (enemy_pos.x - enemy_width / 2 < leftBarrier_pos_x + leftBarrier_width / 2)
		{
			enemy_direction = 1;
			enemyBody.transform.localScale = new Vector3 (-1,
				enemyBody.transform.localScale.y,
				enemyBody.transform.localScale.z);
		}
		else if (enemy_pos.x + enemy_width / 2 > rightBarrier_pos_x - rightBarrier_width / 2)
		{
			enemy_direction = -1;
			enemyBody.transform.localScale = new Vector3(1,
				enemyBody.transform.localScale.y,
				enemyBody.transform.localScale.z);
		}
		enemyBody.transform.Translate(new Vector2(enemy_direction * enemySpeed * Time.deltaTime, 0));
	}

	void Shoot(Vector3 enemy_pos, Vector3 player_pos)
	{
		distance = Vector3.Distance(enemy_pos, player_pos);
		if (distance < shootDistance)
		{
			player_x_y = new Vector2(player.transform.position.x, player.transform.position.y);
			enemy_x_y = new Vector2(enemy.transform.position.x, enemy.transform.position.y);
			direction = player_x_y - enemy_x_y;
 
			ContactFilter2D cf = new ContactFilter2D();
			List<RaycastHit2D> results = new List<RaycastHit2D>();
			Physics2D.Raycast(enemy_x_y, direction, cf, results);

			results.RemoveAll(p => p.collider.tag == "Enemy");
			if (results.Count != 0 && results[0].collider.tag == "Player")
			{
				switch (rnd)
				{
					case 0:
						DeadShot(direction);
						break;
					case 1:
						SnowBallShot(direction);
						break;
					case 2:
						IceShot();
						break;

				}
			}
		}
	}
	public void SnowBallShot(Vector2 dir)
	{
		if (Time.time - timeToNextShot >= AtackDelay)
		{
			animator.Play("HelAngry");
			timeToNextShot = Time.time;
			GameObject bullet = (GameObject)Instantiate(SnowBall);
			bullet.transform.position = enemy_pos;

			float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
			bullet.transform.rotation = Quaternion.AngleAxis(angle + 180, Vector3.forward);

			Physics2D.IgnoreCollision(bullet.GetComponent<Collider2D>(), enemy.GetComponent<Collider2D>());
			Vector3 moveDirection = dir;
			moveDirection.z = 0;
			moveDirection.Normalize();

			bullet.GetComponent<Rigidbody2D>().velocity = moveDirection * 10;
			Destroy(bullet, .7f);
		}
	}

	public void DeadShot(Vector2 dir)
	{
		if (Time.time - timeToNextShot >= AtackDelay)
		{
			animator.Play("FuiradanAngry");
			timeToNextShot = Time.time;
			GameObject bullet = (GameObject)Instantiate(deadShot);
			bullet.transform.position = enemy_pos;

			float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
			bullet.transform.rotation = Quaternion.AngleAxis(angle + 180, Vector3.forward);

			Physics2D.IgnoreCollision(bullet.GetComponent<Collider2D>(), enemy.GetComponent<Collider2D>());
			Vector3 moveDirection = dir;
			moveDirection.z = 0;
			moveDirection.Normalize();

			bullet.GetComponent<Rigidbody2D>().velocity = moveDirection * 15;
			Destroy(bullet, .7f);
		}
	}
	public void IceShot()
	{
		if (Time.time - timeToNextShot >= AtackDelay)
		{
			timeToNextShot = Time.time;
			GameObject bullet = (GameObject)Instantiate(iceShot);
			bullet.transform.position = enemy_pos;

			Physics2D.IgnoreCollision(bullet.GetComponent<Collider2D>(), enemy.GetComponent<Collider2D>());
			Vector3 moveDirection = player_pos.x - enemy_pos.x > 0 ? Vector3.right : Vector3.left;
			bullet.transform.localScale = new Vector3(player_pos.x - enemy_pos.x > 0 ? -1 : 1, 1, 1);
			moveDirection.z = 0;
			moveDirection.Normalize();

			bullet.GetComponent<Rigidbody2D>().velocity = moveDirection * 7;
			Destroy(bullet, .7f);
		}
	}
	void FixedUpdate()
    {
		enemy_pos = enemy.transform.position;
		player_pos = player.transform.position;
		if (isLive)
		{
			Move(enemy_pos);
			Shoot(enemy_pos, player_pos);
		}

		if (Health <= 0 && isLive)
		{
			Death();
		}
    }

	public void Death()
	{
		enemy.SetActive(false);
		isLive = false;
		player.GetComponent<Player>().ChangeCoins(Random.Range(1, 10));
		
		GameObject exp = Instantiate(explosion);
		exp.transform.position = enemy.transform.position;
		Destroy(exp, .5f);

	}
}
