﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Finish : MonoBehaviour
{
    [SerializeField]
    private bool CanUse = false;
    public int RepairItems = 0;
    public int Energy = 0;
    public int MaxEnergy = 100;
    public GameObject boss;
    public bool IsSpawn = false;

    public float lastTimeEnergy;

    private GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        lastTimeEnergy = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (CanUse && Input.GetKey(KeyCode.E))
        {
            if (RepairItems < 3 && gameManager.player.RepairItems > 0)
            {
                gameManager.player.ChangeItems(-1);
                RepairItems++;
            }
            if (RepairItems == 3 && Energy < MaxEnergy)
            {
                if(Time.time - lastTimeEnergy >= 0.3)
                {
                    Energy++;
                    gameManager.player.ChangeEnergy(-1);
                    lastTimeEnergy = Time.time;
                }
            }
            if (!IsSpawn && Energy >= MaxEnergy)
            {
                SpawnBoss();
                IsSpawn = true;
            }
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            CanUse = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            CanUse = false;
        }
    }

    public void SpawnBoss()
    {
        Instantiate(boss, transform);
    }
}
