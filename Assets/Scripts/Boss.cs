﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    public GameManager gameManager;
    private Player player;
    public LineRenderer _lineRenderer;
    public float lastDmg = 0;
    public float lastHit = 0;
    public int Health = 300; 
    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        player = gameManager.player;
        lastHit = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if(player.transform.position.x - transform.position.x < 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        if (Time.time - lastHit > 10f)
        {
            lastHit = Time.time;
            _lineRenderer.positionCount = 0;
        }
        if (Time.time - lastHit > 5f)
        {
            _lineRenderer.positionCount = 2;
            Vector3 dir = player.gameObject.transform.position - transform.position;
            _lineRenderer.SetPosition(0, transform.position);
            RaycastHit2D hit = Physics2D.Raycast(transform.position, dir);
            Debug.Log(hit.collider.tag);
            if (hit.collider)
            {
                _lineRenderer.SetPosition(1, new Vector3(hit.point.x, hit.point.y, transform.position.z));
                if (Time.time - lastDmg > 0.2f && hit.collider.tag == "Player")
                {
                    player.ChangeEnergy(-1);
                    lastDmg = Time.time;
                }
            }
            else
            {
                _lineRenderer.SetPosition(1, dir * 2000);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Bullet")
        {
            Health -= player.Damage;
            if(Health <= 0)
            {
                gameManager.GameOver = 1;
            }
            Destroy(collision.gameObject);
        }
    }
}
